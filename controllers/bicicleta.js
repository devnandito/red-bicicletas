const { removeById } = require("../models/bicicleta");
var Bicicleta = require("../models/bicicleta");

module.exports = {
  list: function (req, res, next) {
    Bicicleta.find({}, (err, bicis) => {
      res.render("bicicletas/index", { bicis: bicis });
    });
  },
  update_get: function (req, res, next) {
    Bicicleta.findById(req.params.id, function (err, bici) {
      res.render("bicicletas/update", { errors: {}, bici: bici });
    });
  },
  update: function (req, res, next) {
    var updateValues = { color: req.body.color, modelo: req.body.modelo };
    Bicicleta.findByIdAndUpdate(req.params.id, updateValues, function (
      err,
      bici
    ) {
      if (err) {
        console.log(err);
        res.render("bicicletas/update", {
          errors: err.errors,
          bici: new Bicicleta({
            color: req.body.color,
            modelo: req.body.modelo,
          }),
        });
      } else {
        res.redirect("/bicicletas");
        return;
      }
    });
  },
  create_get: function (req, res, next) {
    res.render("bicicletas/create", { errors: {}, bici: new Bicicleta() });
  },
  create: function (req, res, next) {
    Bicicleta.create(
      {
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng],
      },
      function (err, newBici) {
        if (err) {
          res.render("bicicletas/create", {
            errors: err.errors,
            bici: new Bicicleta({
              color: req.body.color,
              modelo: req.body.modelo,
              ubicacion: [req.body.lat, req.body.lng],
            }),
          });
        } else {
          res.redirect("/bicicletas");
        }
      }
    );
  },
  delete: function (req, res, next) {
    Bicicleta.findByIdAndDelete(req.body.id, function (err) {
      if (err) {
        next(err);
      } else {
        res.redirect("/bicicletas");
      }
    });
  },
};

// exports.bicicleta_list = function (req, res) {
//   res.render("bicicletas/index", { bicis: Bicicleta.allBicis });
// };

// exports.bicicleta_create_get = function (req, res) {
//   res.render("bicicletas/create");
// };

// exports.bicicleta_update_get = function (req, res) {
//   var bici = Bicicleta.findById(req.params.id);

//   res.render("bicicletas/update", { bici });
// };

// exports.bicicleta_create_post = function (req, res) {
//   const bici = new Bicicleta();
//   bici.color = req.body.color;
//   bici.modelo = req.body.modelo;
//   bici.ubicacion = [req.body.lat, req.body.lng];
//   Bicicleta.add(bici);

//   res.redirect("/bicicletas");
// };

// exports.bicicleta_update_post = function (req, res) {
//   var bici = Bicicleta.findById(req.params.id);
//   bici.id = req.body.id;
//   bici.color = req.body.color;
//   bici.modelo = req.body.modelo;
//   bici.ubicacion = [req.body.lat, req.body.lng];

//   res.redirect("/bicicletas");
// };

// exports.bicicleta_delete_post = function (req, res) {
//   Bicicleta.removeById(req.body.id);
//   res.redirect("/bicicletas");
// };
